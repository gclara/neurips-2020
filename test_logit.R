source("ci_fn.R")

require(NIPS20Submission)
require(varbvs)
require(BinaryEMVS)
require(BhGLM)
require(rstanarm)
require(skinnybasad)
require(matrixStats)

test_logit <- function(n, p, s, X_sd, samples, mag, runif, alg, seed, lambda){

	if (alg %in% c("Lap", "Gauss")){
	    results = matrix(NA, nrow = 9, ncol = samples)
	    names = list(c(paste("TPR", alg, sep = '_'), paste("FDR", alg, sep = '_'), paste("L2", alg, sep = '_'), paste("MSPE", alg, sep = '_'), paste("Time", alg, sep = '_'), paste("Coverage_!0", alg, sep = '_'), paste("Coverage_0", alg, sep = '_'), paste("Length_!0", alg, sep = '_'), paste("Length_0", alg, sep = '_')))
	} else {
	    results = matrix(NA, nrow = 5, ncol = samples)
	    names = list(c(paste("TPR", alg, sep = '_'), paste("FDR", alg, sep = '_'), paste("L2", alg, sep = '_'), paste("MSPE", alg, sep = '_'), paste("Time", alg, sep = '_')))
	}
	for (i in 1:samples){

		#generate sample
		cat("sample ", i, "\n", sep = '')
		if (!is.null(seed)){
			set.seed(seed + i) #advance seed
		}
		X = matrix(rnorm(n*p, 0, X_sd), nrow = n, ncol = p) #design matrix
		theta = numeric(p) #the true signal, called beta in the paper
		if (runif){
		    theta[1:s] = runif(s, -mag, mag)
		} else {
		    theta[1:s] = mag
		}
		probs = 1/(1 + exp(-X %*% theta)) #the true probabilities
		Y = numeric(n)
		for (j in 1:n){
			Y[j] = rbinom(1, 1, probs[j]) #generate the response vector
		}

		if (alg == "Lap"){
		    time = proc.time() #time algorithm with system clock
		    test = test = vb_lap(1000, 1e-4, X, Y, 1, 1, 0, 1/lambda, rep(0,p), rep(1,p), rep(0.5,p))
		    time = proc.time() - time
		    
		    pos = as.numeric(as.numeric(test[[3]]) > 0.5) #algorithm positives
		    post_mean = as.numeric(test[[1]] * test[[3]]) #posterior mean
		    
		} else if (alg == "Gauss"){
		    time = proc.time() #time algorithm with system clock
		    test = test = vb_gauss(1000, 1e-4, X, Y, 1, 1, 0, 1, rep(0,p), rep(1,p), rep(0.5,p))
		    time = proc.time() - time
		    
		    pos = as.numeric(as.numeric(test[[3]]) > 0.5) #algorithm positives
		    post_mean = as.numeric(test[[1]] * test[[3]]) #posterior mean
		    
		} else if (alg == "VarBVS"){
			time = proc.time()
			test = varbvs(X = X, Z = NULL, y = Y, family = "binomial", logodds = 0, optimize.eta = TRUE, verbose = FALSE)
			time = proc.time() - time

			pos = as.numeric(test$pip > 0.5) #algorithm positives
			post_mean = as.numeric(test$beta) * as.numeric(test$pip)
		} else if (alg == "BinEMVS"){
		    time = proc.time()
		    test = BinomialEMVS(y = Y, x = X, type = "logit", b = 1)
		    time = proc.time() - time
		    
		    pos = as.numeric(as.numeric(test$posts) > 0.5) #algorithm positives
		    post_mean = as.numeric(test$betas)
		} else if (alg == "Stan") {
		    time = proc.time()
		    test = stan_glm(formula = Y ~ ., data = data.frame(X, Y), family = binomial(link = "logit"), prior_intercept = NULL, prior = hs())
		    time = proc.time() - time
		    
		    pos = rep(NA, p)
		    post_mean = as.numeric(test$coefficients[-1])
		} else if (alg == "BhGLM") {
		    time = proc.time()
		    test = bglm(formula = Y ~ ., data = data.frame(X, Y), family = binomial(link = "logit"), prior = mde())
		    time = proc.time() - time
		    
		    pos = rep(NA, p)
		    post_mean = as.numeric(test$coefficients[-1])
		} else if (alg == "Skinny") {
		    time = proc.time()
		    test = bglm(formula = Y ~ ., data = data.frame(X, Y), family = binomial(link = "logit"), prior = mde())
		    time = proc.time() - time
		    
		    pos = rep(NA, p)
		    post_mean = as.numeric(test$coefficients[-1])
		} else if (alg == "Skinny"){
		    time = proc.time()
		    test = skinnybasad(scale(X), Y, pr = 0.5, B0 = rep(0,p), Z0 = rep(0,p), nsplit=10, modif=1, nburn=2000, niter=5000, printitrsep=20000, maxsize=30)
		    time = proc.time() - time
		    
		    pos = as.numeric(test$marZ > thresh)
		    post_mean = rep(NA, p)
		}

		pos_TR = as.numeric(theta != 0) #true positives

		results[1,i] = sum(pos[which(pos_TR == 1)])/sum(pos_TR) #TPR
		results[2,i] = sum(pos[which(pos_TR != 1)])/max(sum(pos), 1) #FPR
		results[3,i] = sqrt(sum((post_mean - theta)^2))#L2
		if (alg %in% c("Stan", "BhGLM")) {
		    results[4,i] = sqrt(sum((as.numeric(test$fitted_values) - probs)^2)/n) #MSPE
		} else {
		    results[4,i] = sqrt(sum((1/(1 + exp(-X %*% post_mean)) - probs)^2)/n) #MSPE
		}
		results[5,i] = as.numeric(time[3]) #Time
		if (alg %in% c("Lap", "Gauss")){
		    results[6:9,i] = ci_fn(test$mu, test$sigma, test$gamma, theta, s) #coverage by credible intervals
		}
	}

	return(list(mean = matrix(rowMeans(results), ncol = 1, dimnames = names), sd = matrix(rowSds(results), ncol = 1, dimnames = names)))
}
