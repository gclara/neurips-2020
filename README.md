# Spike and slab variational Bayes for high dimensional logistic regression

Code repository for Ray K., Szabo B., and Clara G. Spike and slab variational Bayes for high dimensional logistic regression. (2020). Advances in Neural Information Processing Systems 33.

## Requirements

To install the dependencies in R:

```setup
install.packages(c("Rcpp", "RcppArmadillo", "RcppEnsmallen"))
```

Navigate to the folder containing the package archive and install:

```install
setwd("/path/to/package/archive")
install.packages("./NIPS20Submission_1.0.tar.gz", repos = NULL)
```

NB: Our algorithms are self-contained with the dependencies listed above. However, re-creating the tables found in the paper via the provided R scripts requires installing additional packages:

```extra
install.packages(c("varbvs", "rstanarm", "BinaryEMVS", "matrixStats"))
```

Additionally, the BhGLM package is available on [GitHub](https://github.com/nyiuab/BhGLM), and the Skinny Gibbs package may be downloaded as part of the supplementary material found [here](https://amstat.tandfonline.com/doi/abs/10.1080/01621459.2018.1482754?journalCode=uasa20).

## Contributing

If you would like to contribute, take a look at the ongoing effort to create an R package with additional algorithms and functionality over at [sparsevb](https://www.gitlab.com/gclara/varpack).
